'''
Created on Aug 27, 2017

@author: mroch
'''

# Standard Python
from collections import namedtuple

# Add on modules

# sortedcontainers module
# http://www.grantjenks.com/docs/sortedcontainers/
# pip install sortedcontainers
import sortedcontainers

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt  # for debugging

import soundfile
import resampy

# Our modules
from dsp.abstractstream import Streamer, StreamGap, StreamEnd
from dsp.timeindex import TimeIndex
from dsp.ringbuffer import RingBuffer


"""
StreamDescriptor is a structure for describing data inside a file and is 
used internally.  Fields are as follows:
file_start - Starting time of file
filename - Path to file
block_starts - A list of timestamps showing the start times of each contiguous
    block of data inside the file.
block_samples - A list of the number of samples associated with each contiguous
    block.  While there is not currently any explicit design for multichannel
    data in this module, this indicates the number of samples of multichannel 
    data, not the number of samples * the number of time intervals over which
    we sample.
Fs - Sample rate
index_to_time - A list of objects of type timeindex that enable converting
    form indices to time.
"""
StreamDescriptor = namedtuple('StreamDescriptor', 
                              ['file_start', 
                               'filename',
                               'block_starts',
                               'block_ends',
                               'block_samples',
                               'Fs',
                               'index_to_time'
                               ])

class TimeNotInStream(ValueError):
    "TimeNotInStream - Exception for bad seek time"
    
    def __init__(self, target, prev_end = None, next_start = None):
        self.target = target
        self.prev_end = prev_end
        self.next_start = next_start
        
    def __repr__(self):
        """"__repr__() 
        Generate string representation showing our target 
        in relation to the stream"""
        
        value = ['time %s is not in the stream'%(self.target)]
        if not self.prev_end is None:
            value.append('Last end:'%(self.prev_end))
        if not self.next_start is None:
            value.append('Next start: '%(self.next_start))
            
        return " ".join(value)

            
class Streams:
    """Streams
    Data stream management.  Maintains a sorted list of timestamped data
    files from the same recording instrument.  All files must be of the same
    sample rate.  Variable bit depths can be supported, but may pose problems
    with respect to calibrating data unless the calibration is done with
    respect to normalized values.  
    
    This class is capable of supporting data gaps and scheduled recording times.
    Analysis to detect times and duty cycles is the caller's responsibility 
    to provide maximal flexibility.  
    """
    # ContiguousData is a type used for reporting contiguous chunks of data
    # that might span files.  See method contiguous_data()
    contig_chunk = namedtuple('ContiguousData',
                              ['start', 'stop', 'start_idx', 'stop_idx'])
        
    def __init__(self):
        self.streams = sortedcontainers.SortedList()
        self.__total_stream_samps = np.uint64(0)
        self.__samps_in_streams = []
        self.last_search_file = None  # File idx last call to timestamp_to_file
        self.last_search_contig = None # Last contiguous segment
        
    def add_file(self, file, samples, timestamps, Fs):
        """add_file(file, samples, timestamps)
        Given a file, provide two lists describing the contiguous
        data segments within the file:
        samples - List of number of samples in each contiguous segment
        timestamps - Start time of each contiguous segment as a pandas timestamp
        Fs - sample rate
        """
        if len(samples) != len(timestamps):
            raise ValueError(
                "sample and timestamp lists must be of same size: {} != {}".format(
                len(samples), len(timestamps)))

        samples = np.uint64(samples)  # Convert to unsigned int 64
        time_indexers = [TimeIndex(t, Fs) for t in timestamps]
        end_timestamps = [idx[s] for idx, s in zip(time_indexers, samples)]
        self.streams.add(StreamDescriptor(
            file_start = timestamps[0], 
            filename = file, 
            block_starts = timestamps, 
            block_ends = end_timestamps,
            block_samples = samples, 
            Fs = Fs, 
            index_to_time = time_indexers))
        
        stream_samples = np.sum(samples)
        # document the number of samples in the entire stream
        self.set_total_stream_samps(self.get_total_stream_samps() 
                                    + stream_samples)
        self.set_samps_in_streams(stream_samples)
        
    def contiguous_data(self):
        """contiguous_data_report() - Return information about contiguous data
        Returns array of named ContiguousData tuples.  Each tuple contains:
        .start - start time
        .stop - end time
        .start_idx - (fidx, cidx)
        .stop_idx - (fidx, cidx)
        Where fidx is file self.streams[fidx].filename and
        cidx is the c'th contiguous data chunk within that file.
        
        Caveat:  If add_file is called after calling contiguous_data, the
        results are no longer valid.
        """
        
        contigs = []    # List of contiguous data segments
        
        sidx = 0    # stream index
        cidx = 0    # file contig index
        
        # prime loop
        # Set start and end of current contig block and remember indices
        # as start of first contiguous data chunk. 
        start = self.streams[sidx].block_starts[cidx]
        current_end = self.streams[sidx].block_ends[cidx]        
        first_sidx = sidx   # Remember current stream & contiguous data indices
        first_cidx = cidx
        prev_sidx = 0
        
        more = True
        while more:
            # Look at next contig
            prev_cidx = cidx
            cidx = cidx + 1
            if cidx >= len(self.streams[sidx].block_ends):
                # Went past last contig in this file, prepare for next file
                cidx = 0
                prev_sidx = sidx
                sidx = sidx + 1
                more = sidx < len(self.streams) # Any more files?

            if not more:
                # This was the last file, store the contig
                contigs.append(self.contig_chunk(
                    start,
                    self.streams[prev_sidx].block_ends[prev_cidx],
                    (first_sidx, first_cidx),
                    (prev_sidx, prev_cidx)))
            else:
                new_start = self.streams[sidx].block_starts[cidx]
                old_end = self.streams[prev_sidx].block_ends[prev_cidx]
                if new_start > old_end:
                    # Store the contig we just went by and mark the
                    # current one as a new contig
                    contigs.append(self.contig_chunk(
                        start, old_end, 
                        (first_sidx, first_cidx), 
                        (prev_sidx, prev_cidx)))
                    start = new_start
                    first_sidx = sidx
                    first_cidx = cidx
        
        return contigs
                    
                
                                   
        
    def get_samps_in_steams(self):
        '''return list of the number of samples in each stream'''
        return self.__samps_in_streams
    
    def set_samps_in_streams(self, x):
        '''returns a list indicating the end sample of each stream '''
        if len(self.get_samps_in_steams()) == 0:
            new_x = x
        else:
            new_x = x + self.get_samps_in_steams()[-1]
        self._Streams__samps_in_streams.append(new_x)
        
        
    def get_total_stream_samps(self):
        ''' Returns the total number of samples in the entire samplestream'''
        return self._Streams__total_stream_samps
    
    def set_total_stream_samps(self, x):
        self.__total_stream_samps = x

    
    
    def __len__(self):
        return len(self.streams)
    
    def __iter__(self):
        return iter(self.streams)
    
    def __getitem__(self, key):
        return self.streams[key]
    
    def get_stream(self, idx):
        
        return SampleFile(self.streams[idx])
    
   
    def timestamp_to_file_offset(self, timestamp, debug=False):
        """timestamp_to_file_offset(timestamp)
        Convert timestamp to a file, contiguous data section and sample offset 
        relative to the start of file
        
        Returns 4 tuple (infile, incontig, fileoffset, contigoffset)
        infile - Index of stream element corresponding to a file
        incontig - Index of the Nth block of contiguous data within a file
        fileoffset - Number of samples into file #infile
        contigoffset - Number of samples into contiguous data segment #incontig

        If there are no data at the specified time, raises a TimeNotInStream
        exception
        """
        
        # Each stream is represented by a StreamDescriptor tuple
        # See comments in namedtuple definition for StreamDescriptor above
        
        streamsN = len(self.streams)
        if streamsN == 0:
            raise TimeNotInStream(timestamp)     # no data case
        
        if self.last_search_file is None:
            self.last_search_file = int(streamsN / 2)  # start search in ~ middle
            
        # binary search for correct file
        filefound = False
        fidx = self.last_search_file
        first_fidx = 0
        last_fidx = streamsN - 1
        while not filefound:
            filestart = self.streams[fidx].file_start
            fileend = self.streams[fidx].block_ends[-1]  # Last end time
            if debug:
                print("file tgt (idx=time): %d=%s [%d=%s, %d=%s]"%(
                      fidx, timestamp, first_fidx, filestart, last_fidx, fileend))
            if timestamp < filestart:
                # Before current file 
                last_fidx = fidx - 1
                if last_fidx < 0:
                    self.report_badtime(timestamp, fidx)
            elif timestamp >= fileend:
                first_fidx = fidx + 1
                if first_fidx > last_fidx:
                    self.report_badtime(timestamp, fidx)
            else:
                filefound = True
                
            if not filefound:
                fidx = int((first_fidx + last_fidx) / 2)
            
        # We should not get here without finding a file spanning the timestamp
        contigsN = len(self.streams[fidx].block_samples)
        if fidx != self.last_search_file or self.last_search_contig == None:
            # Not the same file we searched in last time, set the index into
            # the contiguous blocks to the midpoint.
            self.last_search_contig = int(contigsN / 2)
            
        # Binary search of contiguous segments
        contigfound = False
        first_cidx = 0
        last_cidx = contigsN - 1
        cidx = self.last_search_contig
        while not contigfound:
            contigstart = self.streams[fidx].block_starts[cidx]
            contigend =  self.streams[fidx].block_ends[cidx]
            if debug:
                print("contig tgt %s [%d=%s, %d=%s"%(timestamp, first_cidx, contigstart, last_cidx, contigend))
            if timestamp < contigstart:
                # Restrict search to contigs before this one
                last_cidx = cidx - 1
                if last_cidx < 0:
                    raise RuntimeError("Search before first contiguous data segment should not reach this point")
            elif timestamp > contigend:
                # Restrict search to contigs after this one.
                first_cidx = cidx + 1
                if first_cidx > contigsN:
                    raise RuntimeError("Search after last contiguous data segment should not reach this point")
            else:
                contigfound = True  # timestamp inside contiguous data segment                
                
            if not contigfound:
                next_cidx = int((first_cidx + last_cidx) / 2)
                if next_cidx == cidx:
                    self.report_badtime(timestamp, fidx, cidx)
                else:
                    cidx = next_cidx
                    
        # If we made it here without throwing an exception, we know the
        # file, and its contig.  Compute the number of samples into the block
        offset = timestamp - self.streams[fidx].block_starts[cidx]
        offset_s = offset.total_seconds()
        offset_samples = np.uint64(offset_s * self.streams[fidx].Fs + 0.5)
        # Compute number of samples into the file
        # Do not include the number of samples in the current contig as we
        # want the start of the contig.
        base_samples = np.sum(self.streams[fidx].block_samples[:cidx])
        samples_into_file = base_samples + offset_samples
        
        self.last_search_file = fidx
        self.last_search_contig = cidx
        
        return (fidx, cidx, samples_into_file, offset_samples)

    def get_filename(self, timestamp):
        """get_filename - Given a timestamp, report the name of
        the file associated with the timestamp.
        :param timestamp:
        :return: filename
        """
        fidx, _, _, _ = self.timestamp_to_file_offset(timestamp)
        return self.streams[fidx].filename

    def report_badtime(self, timestamp, fidx, cidx=None):
        """report_badtime(timestmap, fidx, cidx)
        Given a timestamp and the last timespan searched, generate a
        TimeNotInStream exception 
        """
        
        prev_end = None
        next_start = None
        if cidx is None:
            # Problem was at file level
            if fidx <= 0:
                if timestamp < self.streams[0].file_start:
                    next_start = self.streams[0].file_start
                else:
                    prev_end = self.streams[0].block_ends[-1]
            else:
                # Check if time before or after specified file
                if timestamp < self.streams[fidx].file_start:
                    # Reort start time of this file and end of previous 
                    next_start = self.streams[fidx].file_start
                    if fidx > 0:  # if applicable...
                        prev_end = self.streams[fidx-1].block_ends[-1]
                else:
                    prev_end = self.streams[fidx].block_ends[-1]
                    if fidx+1 < len(self.streams):
                        next_start = self.streams[fidx+1].file_start
                    else:
                        next_start = None
                        
            raise TimeNotInStream(timestamp, prev_end, next_start)
        else:
            # timestamp was between contiguous segments
            contig_start = self.streams[fidx].block_starts[cidx]
            contig_end = self.streams[fidx].block_ends[cidx]
            if timestamp < contig_start:
                raise TimeNotInStream(timestamp, 
                                prev_end=self.streams[fidx].block_ends[cidx-1], 
                                next_start=contig_start)
            else:
                raise TimeNotInStream(timestamp,
                                prev_end=contig_end,
                                next_start=self.streams[fidx].block_starts[cidx+1])

        
class SampleFile:
    "An instance of a stream"
    
    def __init__(self, streamtuple):
        """Create a stream object for reading data
        streamtuple must have:
            
            [0] - Starting timestamp for file
            [1] - Filename
            Lists describing contiguous sections of data within file
 
            These lists are of the same size
            [2] - Start time of contiguous data block
            [3] - Number of samples in contiguous block
 
        """
        # streamtuple
        self.filename = streamtuple.filename
        self.audio = soundfile.SoundFile(self.filename)
        
        self.Fs = self.audio.samplerate
        #self.samplesN = np.uint64(self.audio.frames)
        self.samplesN = len(self.audio)
        self.channels = self.audio.channels
        self.format = self.audio.format
        self.subtype = self.audio.subtype
        # Determine how many bits/sample if possible
        subtypes = {'PCM_24' : 24, 'PCM_16': 16, 'PCM_8' : 8}
        if self.subtype in subtypes:
            self.bits = subtypes[self.subtype]
        else:
            self.bits = None  # unknown
        
        self.times = streamtuple.index_to_time
        self.samples = streamtuple.block_samples
            
        self.contig_idx = 0  # nth contiguous block
        
        # When a transition event is about to occur such as the end
        # of a contiguous block or the end of file, the event is populated
        # with the exception that will be raised. 
        self.event = None
        
        # Store the index into the current contiguous block, aka "contig"
        # as well the number of samples in the current contig
        self.index = np.uint64(0)
        self.contig_samples = self.index + self.samples[self.contig_idx]
        
        # Store the starting sample into the stream for each contig
        if len(self.samples) > 1:
            cumulative = np.cumsum(self.samples)
            self.contigs_first_sample = np.stack(
                [[np.uint64(0)], cumulative[0:-1]])
        else:
            self.contigs_first_sample = np.zeros([1], dtype="uint64")        
    
    def read(self, N):
        """"read(N) - Read N samples

        Returns tuple:
            0 : sample data
            1 : starting index within the current contiguous block
            2 : timestamp of the first sample
            3 : index of contiguous block
            
        When approaching the end of a contiguous block, fewer than N samples
        will be returned.  
        
        Whenever a read results in reaching the end of the set of files
        or the end of a contiguous data block, a subsequent read will
        produce a StreamGap (more data can be obtained by subsequent
        calls to read) or EndOfStream in which case there are no
        more data to be had.
        """

        
        if self.event:
            # We have reached a StreamGap or the end of file
            event = self.event
            if isinstance(event, StreamGap):
                # There's more to read, just let caller know that their next
                # read is not contiguous
                self.event = None
                self.index = np.uint64(0)  # Starting a new block
                print("File {} Contig {} index set to {}".format(
                    self.filename, self.contig_idx, self.index))

            raise event
        
        # Store index of current contiguous block
        # (will be updated if we read to the end)
        contig_idx = self.contig_idx
        
        if self.index + N >= self.contig_samples:
            # We will reach or go past the end of block
            
            # Find out how much data we can read (might be everything the
            # caller wanted if we read up to the boundary

            new_N = int(self.contig_samples - self.index)

            data = self.audio.read(new_N)
            
            # Next read should produce some type of event
            self.event = self._next_contig()
                
        else:  
            data = self.audio.read(N)
            
        # Single channel data needs to be reshaped to be consistent
        if len(data.shape) == 1:
            data = np.reshape(data, [data.size, 1])

        data_N = np.uint64(data.shape[0])  # number samples read
        
        # Compute start time of sample
        timestamp = self.times[contig_idx][self.index]
        startidx = self.index
        self.index += data_N
        return (data, startidx, timestamp, contig_idx)
    
    def set_next(self, contigidx, samples_into_contig):
        """
        Set the next read to be from the specified contig index
        at the specifed number of samples into the contig
        :param contigidx: Index of contiguous data segment
        :param samples_into_contig: Samples inot contiguous data segment
        """
        self.contig_idx = contigidx
        self.contig_samples = self.samples[contigidx]
        self.index = samples_into_contig

    def _next_contig(self):
        """_next_contig() - Prepare next contiguous block.  Returns one of the
           following:
            EOF - Next read will go past the end of file
            StreamGap - Next read will start a new contiguous section
        """
        
        self.contig_idx = self.contig_idx + 1
        if self.contig_idx < len(self.samples):
            # More to read
            self.contig_samples = np.uint64(self.samples[self.contig_idx])
            value = StreamGap(filename=self.filename, 
                            samples=self.contigs_first_sample[self.contig_idx],
                            contig=self.contig_idx, 
                            timestamp=self.times[self.contig_idx][0])
        else:
            value = EOFError(self.filename)
            #print("SampleFile.read - EOF {}".format(value))
            
        return value
    
    def next_sample_idx(self):
        "next_sample_idx() - Return sample that will be read next"
        return self.contig_samples
    
    def next_timestamp(self):
        """next_timestamp() - Return the timestamp of the next sample to be read
        When the end of file is reached, the time returned is one sample
        past the last one read.
        """
        if self.event != None and isinstance(self.event, EOFError):
            # next read is past this file
            # Return time of next sample past end
            t = self.times[-1][self.samples[-1] + 1]
        else:
            t = self.times[self.contig_idx][self.index]
        return t
    
    def start_timestamp_contigN(self, idx):
        "start_timestamp_contigN(N) - Return starting time Nth contig"
        return self.times[idx][0]
    
    def samplefile_contiguous(self, start_time):
        """"samplefile_contiguous(start_time) 
        Input: start time- the start time of the NEXT file
        Given the start_time of another file, does it continue this one
        without a StreamGap?
        Returns True or False
        """
        expected = self.times[-1][self.samples[-1]]        
        return expected == start_time
    
    def at_end_of_file(self):
        "at_end_of_file() - Will the enxt read result in end of file?"
        return isinstance(self.event, EOFError)
    
    def at_stream_gap(self):
        "at_stream_gap() - Will the next read result in a stream gap?"
        return isinstance(self.event, StreamGap)
        
            
        
    
class SampleStream(Streamer):
    """
    SampleStream
    
    Class that merges multiple files with the same characteristics (sample rate,
    quantization, etc.) into a single stream. The stream may have gaps both
    across and within files. See constructor for gap specification.

    """
    def __init__(self, streams, targetFs=None, maxhistory=40000):

        """AudioSamples(streams, targetFs, maxhistory)
        Create a stream of audio samples. 

        :param streams: Streams object describing files to be bound to a
           data stream
        :param targetFs: If present, data will be resampled to the target
           sample rate.  No padding is provided, so there will be edge
           effects.  Use this prudently.
        :param maxhistory:  Maximum number of samples retained
        maxhistory - Maximum number of history samples to retain
        
        CAVEATS:  The read functions are in samples and this number always
        refers to the source data sample rate, not the targetFs when
        resampling is done.  Conversion is only done on the returned data.
        """

        self.streams = streams
        
        # offsets into stream
        self.stream_idx = 0   # current stream
             
        # Set up first audio file
        self.stream = self.streams.get_stream(self.stream_idx)
        
        # initialize history buffer

        self._hist_size = maxhistory
        self._hist = RingBuffer(maxhistory, self.stream.channels)
        self._reset_hist()
        
        self.channel_select = self.stream.channels   # channel select:  All channels by default
        self.allchan = True
        
        self._readtype = "int16"  # change to float64 after debugged

        self.targetFs = targetFs

    def _reset_hist(self):
        "_reset_hist - reset history buffer"
        self._hist.clear()
        # Our next read must be inside a file, so reset any
        # pending StreamGap or StreamEnd event
        self.event = None
             
    def _next_stream(self):
        """_next_stream() - Prepare to read from next stream
        Returns:
            None - Data are contiguous, can continue reading
            StreamGap() - Return any data read so far, but the next
                read should produce a StreamGap exception
            StreamEnd() - Return any data read so far, but the next
                read should produce an StreamEnd excpetion
        """
        if self.stream_idx + 1 < len(self.streams):
            # Is there a gap?
            contig_continues = self._contig_continues_next_file()
#            # debug
#            if not contig_continues:
#                xyzzy = self._contig_continues_next_file()
#                
            
            # Move to next stream 
            self.stream = self.streams.get_stream(self.stream_idx+1)
            
            if contig_continues:
                action = None
            else:
                # Currently don't track exact position in samples
                # Update this if we start doing so.  For now, first
                # sample in next file
                action = StreamGap(
                    samples = self.stream.next_sample_idx(),
                    timestamp = self.stream.next_timestamp(),
                    contig = self.stream.contig_idx,
                    filename = self.stream.filename)
            self.stream_idx = self.stream_idx + 1

        else:
            # Reached end of stream, no more files to process
            action = StreamEnd()
        return action

    def get_channelsN(self):
        "get_channelsN() - Return Number of available channels"
        # Returns number of channels in the current stream
        # All streams within the sample stream should be homogeneous
        return self.stream.channels

    def get_stream_len(self):
        return len(self.streams)
    

    def get_all_stream_filenames(self):
        '''
        Returns a list of the filename of each stream in the streamlist
        '''
        
        filenamelist = []
        
        for ii in range(len(self.streams)):
            filenamelist.append(self.streams[ii][1])
            
        return filenamelist


    def get_all_stream_fs(self):
        '''
        Returns a list of the sample rate in each file in the stream
        '''
        
        file_fs = []
        
        for ii in range(len(self.streams)):
            file_fs.append(self.streams[ii].Fs)
            
        return file_fs
    
    def get_all_stream_samps(self):
        '''
        Returns a numpy array indicating the number of samples (total) in each
        file
        '''
        
        sampN = np.zeros(self.get_stream_len())
        
        for ii in range(len(self.streams)):
            sampN[ii] = self.streams[ii][3].block_samples
            
        return sampN
        
            
    def _contig_continues_next_file(self):
        """"_contig_continues_next_file() - Return True if the current
        SampleFile's contiguous section continues in the next file
        """
        next_stream = self.stream_idx + 1
        if next_stream  < len(self.streams):
            # See if start time of next stream is consistent
            # with the end of this stream's time
            next_start = self.streams[next_stream][0]
            result = self.stream.samplefile_contiguous(next_start)
        else:
            result = False  # no more, doesn't continue
        return result
         
        
    def read(self, N, previousN=None, _previous=None):
        """read(N, previousN=None, _previous=None)
        Read N samples from the stream.  
        
        If previousN is specified, one of two things will occur:
        
            If this is not the first read of the stream or after a StreamGap, 
            at most previousN samples will be prepended to the returned data.
            (Number prepended depends on how many previous samples are
             available)
            
            If it is the first read, N+previousN samples will be read
            
        Argument _previous is for internal use only.
        
        May raise the following exceptions:
            StreamGap - End of a contiguous block of data was reached. Next read
                will start a new non-contiguous block of data.  Examples of 
                events that can cause data gaps are recorder servicing, 
                duty cycles, event triggers, etc.
                StreamGap events resent the history and previous bytes 
                will no longer be available.
                
            StreamEnd - No more samples are available

        :return: (data, idx, tstamp, contig_idx, stream_idx)
            data - signal samples
            idx - sample index within the current contiguous block
            tstamp - timestamp of first sample read
            contig_idx - Nth contiguous block in current stream
            stream_idx - Read from the specified index (file) in the stream
        """

        # About to read across a StreamGap or past StreamEnd?
        # Raise the exception
        # Note that StreamGap can be raised within a SampleFile read,
        # this handles StreamGaps across files
        if self.event != None:
            event = self.event
            self._hist.clear()  # No history available across a gap
            # No history is available across a gap
            self._reset_hist()

            print("Read triggered {}".format(event))            
            raise event
        
        # If user wanted history and there is not enough history
        # available, increase samples to read
        availhist = len(self._hist)
        if previousN != None:
            missing = previousN - availhist
            if missing > 0: 
                N = N + missing  # increase samples to read
                previousN = availhist  # Only use available history
        Fs = self.stream.Fs  # sample rate of these data
        (data, idx, tstamp, contig_idx)= self.stream.read(N) #dtype=self._readtype)
        readN = data.shape[0]

        # How many of the read samples can be stored to the history?        
        storeN = min(readN, availhist)
        
        if previousN:
            previous_data = self._hist.last(previousN)

        self._hist.enqueue(data) # add read data to history

        # Update what was read if needed
        if previousN:
            data = np.vstack([previous_data, data])
        
        if self.stream.at_end_of_file():
            # We have reached the end of the current stream.
            
            # Move to the next stream
            self.event = self._next_stream()
            # The next stream might be a continuation 
            
            if self.event == None:
                # Next stream is a continuation.
                if N > readN:
                    # Caller requested more than we read, get the rest
                    # We adjust the number of samples used from the history
                    # in a new call
                    if previousN == None:
                        # Use the samples we just read
                        previousN = readN
                    else:
                        # Use the samples we just read and the history the user
                        # already requested
                        previousN = previousN + readN
                    new_read = N - readN
                    return self.read(int(new_read), previousN=previousN)

        if self.targetFs is not None and self.targetFs != Fs:
            data = resampy.resample(data, Fs, self.targetFs, axis=0)

        # Return data, the sample number, timestamp, contiguous data index
        # within the stream.  Sample idx will be invalid when sample rate
        # conversion is conducted.
        return (data, idx, tstamp, contig_idx, self.stream_idx)
    
    def set_time(self, timestamp):
        """set_time(timestamp) - Given a Panda timestamp, 
        seek to the appropriate time.  
        """
        
        # Determine where timestamp lies:
        # file_idx - which file
        # contig_idx - which contiguous block within the file
        # samples_into_contig - # samples into file fileidx in specified contig
        file_idx, contig_idx, samples_into_file, samples_into_contig = \
            self.streams.timestamp_to_file_offset(timestamp)
        
        # update stream idx and the sample location 
        self.stream = self.streams.get_stream(file_idx)
        self.stream_idx = file_idx
        self.stream.set_next(contig_idx, samples_into_contig)
        # Reset the history
        self._reset_hist()
        
        #check seek time and move the cursor using audio.seek
        if self.stream.audio.tell() != self.stream.index:
            self.stream.audio.seek(samples_into_file)

    
        # set the (*&)^ stream index
        self.stream_idx = file_idx
                
                
    def set_sample(self, sample):
        """set_sample(int) - Given sample ID, 
        seek to the appropriate time.  
        
        """
    
        # This looks like code that was started but never completed.    
        raise NotImplemented
    
#         temp = np.asarray(self.streams.get_samps_in_steams())
#         temp_stream_idx = np.argmax(temp>sample)
#         
#         if temp_stream_idx == 0:
#             
# 
#             self.stream = self.streams.get_stream(temp_stream_idx)
#             self.stream.index = int(sample_into_file)           
#         else:
#             sample_into_file = sample- temp[temp_stream_idx-1]
#             
#             # Else update which stream and the sample location 
#             self.stream = self.streams.get_stream(temp_stream_idx)
#             self.stream.index = int(sample_into_file)
#             self.stream_idx = temp_stream_idx
#         
#         # Reset the history
#         self._reset_hist()
#         
#         # set the (*&)^ stream index
#         self.stream_idx = temp_stream_idx
#         
#         #check seek time and move the cursor using audio.seek
#         if self.stream.audio.tell() != self.stream.index:
#             self.stream.audio.seek(sample_into_file)    
#             
#   
#         # Seek to appropriate sample  
        
    
    def time_has_data(self, timestamp):
        """time_has_data(timestamp)
        Checks to see if the stream has data at the specified timestamp.
        If data exist, returns True, otherwise returns an instance of class
        TimeNotInStream that details when (and if) data exist before or
        after the specified time.
        """
        
        try:
            [_fidx, _cidx, _foffset, _coffset] = \
                self.streams.timestamp_to_file_offset(timestamp)
        except TimeNotInStream as e:
            return e
        
        return True
            
              
    def set_channel(self, channels):
        """set_channel(channels) - Iterators return only specified channel(s)
        By default, all channels are returned.
        
        Select a set of channels to be returned by:
            list, e.g. [0, 1, 5]
            scalar, 0    Use total number of channels to specify all channels
        
        CAVEAT:  Setting channel will change the the size and shape reported
        """
        
        raise NotImplementedError("Channel selection code is not functional YET. Needs implementation.")
        
        if channels == None:
            raise ValueError("channels must be specified")
        if isinstance(channels, int):
            if channels == self.stream.channels:
                # special case, all channels
                self.channel_select = self.stream.channels
                channels = list([x for x in range(self.stream.channels)])
            else:            
                channels = list(channels)
                
        for c in channels:
            if c < 0 or channels >= self.stream.channels:
                raise ValueError(
                    "Bad channel specification list, must be in [0,{}] ".format(
                        self.stream.channels-1))
        self.channel_select = channels
        if len(set.intersection(set(channels),
                                set([x for x in range(self.stream.channels)]))
               ) == self.stream.channelsN:
            self.allchan = True
        else:
            self.allchan = False
    
    def get_stream_event(self):
        """get_steam_event() - Indicate if the last read prepared us for a 
        stream event.
        Event codes:
            None - Can likely continue to read
            StreamGap() - End of contiguous data was reached  during
                last read.  Next read will trigger a StreamGap exception
            StreamEnd() - End of data stream has been reached.  Next
                read should produce an StreamEnd exception
                
        CAVEAT:  If a read ended on a stream boundary, i.e. the user was able to
        read all the data they requested, the stream event will not be set, so
        this is not a good way to check if the next read will trigger an
        exception.  It is useful for checking why the last read did not return
        as much data as was expected.
        """
        
        event = None
        if self.event:
            event = self.event
        else:
            # The current stream may have triggered the event
            gap = self.stream.at_stream_gap()
            # At stream end - Need to double check how this works, we should really set up
            # for an EndOfStream when we hit this condition.
            lasteof = self.stream.at_end_of_file() and self.stream_idx >= len(self.streams)
            if lasteof:
                print("Last EOF, should be StreamEnd instead... investigate")
            if gap or lasteof:
                event = self.stream.event
                 
        return event

    def get_current_Fs(self):
        "Return current sample rate"
        return self.stream.Fs

    def get_current_bitdepth(self):
        """
        Samples are normalized to [-1, 1] by the underlying libraries
        This returns the number of bits used for the sample in the current
        stream.  To restore a sample to its quantized value, multiply the
        sample by 2^(bits - 1).
        """
        return self.stream.bits

    def get_current_timestamp(self):
        ''' returns the datetimestamp at the current sample'''
    
        # timestamp at start of current file
        file_timestamp_start = self.stream.times[0][0]
        
        # Samples into file
        samp_number = self.stream.audio.tell()
        
        # fs of the sample stream
        fs = self.stream.Fs
        
        #seconds into file
        seconds_into_file = samp_number/fs
        
        # add the seconds to the timestamp and return it        
        timestamp = file_timestamp_start +\
            pd.Timedelta(seconds=seconds_into_file)
        
    
        return timestamp
    

    def get_current_sample(self):
            ''' returns the number of samples into the streamer'''
                    
            sample_into_stream = np.sum(self.get_all_stream_samps() \
                                        [0:int(self.stream_idx)]) +\
                                        self.stream.audio.tell()
            return(sample_into_stream)



























