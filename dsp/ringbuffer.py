
# site-library modules
import numpy as np

class RingBuffer:
    def __init__(self, capacity, dim=1, dtype=np.float64):
        """
        Create a circular ring buffer
        :param capacity: Number of elements in buffer
        :param dim: Dimension of vectors
        :param dtype: numpy data type
        """

        # front and back indices of circular buffer
        self.front = 0
        self.back = 0

        # Number of items in buffer N and buffer capacity
        # We waste one slot so that front==back indicates empty
        self.N = capacity+1
        self.capacity = capacity

        # buffer
        self.buf = np.zeros((capacity+1, dim), dtype=dtype)

        # Used for dequue on an empty queue
        self.emptyvec = np.zeros((0,dim), dtype=dtype)

    def isempty(self):
        "isempty() - True if buffer contains no data"
        return self.front == self.back

    def isfull(self):
        "isfull() - True if buffer is at capacity"
        return (self.back + 1) % self.N == self.front

    def get_capacity(self):
        "get_capacity() - Return maximum capacity of ring buffer"
        return self.capacity

    def clear(self):
        "clear() - Clear buffer"
        self.front = 0
        self.back = 0

    def enqueue(self, values):
        """
        enqueue(values)
        :param values:  Numpy array of values to be added to buffer
        """

        n = values.shape[0]

        if n > self.capacity:
            raise ValueError("%s has %d elements, buffer only has %d"%(
                __name__, n, self.capacity))

        # If the buffer is full, we'll need to discard some of the oldest
        # samples
        bufN = len(self)
        # If we overflow the buffer, we'll need to advance the front
        # and discard samples
        advance_front = max(0, bufN + n - self.capacity)

        # Truncate value range if too big
        if n > self.capacity:
            valuestart = n - self.capacity
            n = self.capacity
        else:
            valuestart = 0

        bufend = self.back + n  # one past last index
        if bufend > self.N:
            # Wraps around buffer
            wrappt = self.N - self.back
            self.buf[self.back:,:] = values[valuestart:wrappt,:]
            bufend = bufend % self.N
            self.buf[0:bufend, :] = values[wrappt:,:]
        else:
            self.buf[self.back:bufend,:] = values

        self.back = bufend
        if advance_front > 0:
            self.front = (self.front + advance_front) % self.N

    def __len__(self):
        "len() - Remote lenth of items in buffer"
        # determine current number of objects
        wrapped = self.front > self.back
        if wrapped:
            N = self.N + self.back - self.front
        else:
            N = self.back - self.front
        return N

    def dequeue(self, itemsN):
        """
        dequeue(itemsN)
        :param itemsN:  Number of items to remove from the head of the list
        :return: numpy view of buffer.  This is a pointer, and not thread safe
            if multiple threads are using the queue instance.  If n items are
            not available, returns
        """

        N = len(self)
        if N < itemsN:
            raise ValueError("%s contains %d objects, tried to dequeue %d"%(
                self.__class__, N, itemsN))

        # valid request, set up indices to pull data
        if self.front + itemsN < self.N:
            # simple case, pull data
            slice = tuple(range(self.front,self.front+itemsN))
        else:
            # dequeue wraps around buffer
            firstN = self.capacity - self.front
            slice = list(range(self.front,self.capacity))
            lastN = itemsN - firstN
            slice.extend(tuple(range(lastN)))

        #print("front %d back %d, take %d"%(self.front, self.back, itemsN))
        #print(slice)

        data = np.take(self.buf, slice, axis=0)
        self.front = (self.front + itemsN) % self.N
        return data

    def last(self, itemsN):
        """
        last(itemsN) - Return last itemsN of history without changing the
        buffer history.  Good for taking overlapping samples in framing, e.g.
        a 5 ms advance in a 20 ms window where we need to access the same data
        multiple times.  This is a peek into end of the queue type operation
        :param itemsN:
        :return: itemsN data
        """

        N = len(self)
        if N < itemsN:
            raise ValueError("%s contains %d objects, tried to dequeue %d"%(
                self.__class__, N, itemsN))

        # valid request, set up indices to pull data
        first = self.back - itemsN
        if first >= 0:
            # simple case, pull data
            slice = tuple(range(first,self.back))
        else:
            # wraps around buffer
            slice = list(range(-first))
            slice.extend(tuple(range(self.back+first, self.capacity)))

        #print("front %d back %d, take %d"%(self.front, self.back, itemsN))
        #print(slice)

        data = np.take(self.buf, slice, axis=0)

        return data

if __name__ == '__main__':

    # Test RingBuffer
    for s in range(4,5):
        ring = RingBuffer(5*s, s)
        for idx in range(1,25):
            x = np.eye(s)*idx  # square matrix with idx on the diagonal
            ring.enqueue(x)

            print("%d: %d Items in buffer of size %e"%(
                idx, len(ring), ring.get_capacity()))

            if (idx % 3) == 0:
                print("checking last %d: front %d back %d"%(
                    s, ring.front, ring.back))
                data = ring.last(s)
                print("%d items"%(s))
                print(data)
