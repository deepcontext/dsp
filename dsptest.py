#sys.path.append("/home/kpalmer/AnacondaProjects/dsp")
import unittest

from dsp.FrameStream import AudioFrames
from dsp.FrameStream import soundfile_avail
from dsp.abstractstream import StreamGap

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from dsp.RMSStream import RMSStream
from audioop import rms
from dsp.DFTStream import DFTStream





#from dsp.MultiStream import MultiStream
import dsp.SampleStream
import dsp.timeindex

import soundfile

class TestAudio(unittest.TestCase):
    
    testfiles = ["test_wav/test100toNeg100.wav",
                 "test_wav/test100toNeg100_16b-2ch.wav",
                 "test_wav/test100toNeg100_16b-3ch.wav",
                 # libsndfile appears to be scaling 24 bit as 16, investigate later
                 "test_wav/test100toNeg100_24b.wav"
                 ]
    bitwidths = [16, 16, 16, 24]  # bits in each file
    
    ch1 = [x + 1 for x in range(100)]
    ch1.extend([x for x in range(99,-101,-1)])
    ch2 = [x for x in range(-100,101)]
    ch2.extend([x for x in range(99,0,-1)])
    
    
    
    testvalues = [
        np.column_stack([np.array(ch1, np.float)]),
        np.column_stack([np.array(ch1, np.float), np.array(ch2, np.float)]),
        np.column_stack([np.array(ch1, np.float), np.array(ch2, np.float), np.array(ch1, np.float)]),
        np.column_stack([np.array(ch1, np.float), np.array(ch2, np.float)]),
        ]
    
    def checkAudioFrames(self, adv_ms = 10, len_ms = 20,
                         incomplete = False, interface = "SoundFile"):
        """checkAudioFrames - check framing
            incomplete - Use incomplete frames (True|False)
            interface - class to use for testing.  Valid choices:
                "SoundFile", "SoundFileStream", "wavfile"
        """
                   
        print("\nVerifying framing using module {}, adv_ms {} len_ms {} incomplete {}".format(
            interface, adv_ms, len_ms, incomplete))
            
        success = True # victory until we fall on our face...
        fileidx = 0
        failed = {}
        for f in self.testfiles:
            
            # scipy does not yet support 24 bit files
            # also skipping for libsndfile as it appears to be using 16
            # scaling (at least on Windows) although it does read 24 bit files 
            if self.bitwidths[fileidx] == 24:
                print("Skipping 24 bit file")
                continue
            
            print("{} checking framing".format(f))
            framer = AudioFrames(f, adv_ms, len_ms, interface=interface, incomplete=incomplete)
            
            # Set up parameters for first frame
            start = 0  # Seek to N+1th sample 
            if start:
                framer.seek_sample(start)
            
            advance = framer.adv_N
            length = framer.len_N
            framelens = []
            
            # print(framer.get_params())
            
            frameidx = 0
            showN = min(3, length)  # When entire frames are different show first/last N
            offsets = []
            for (f, offset_s, timestamp) in framer.__iter__():
                if interface in ["SoundFile", "SoundFileStream"]:
                    # Soundfile normalizes by default.  Convert to decimal
                    f = f * np.power(2, self.bitwidths[fileidx]-1)
                framelens.append(f.shape[0])
                offsets.append(offset_s)
                
                if len(f.shape) == 1 and len(f) > 0:
                    # Make sure column vector
                    f = np.reshape(f, [len(f), -1])
                    
                expected = self.testvalues[fileidx][start:start+f.shape[0],:]
                
                # Introduce an error to verify testing routine (leave False unless
                # testing test routines
                if False:
                    if frameidx == 0:
                        expected[max(16, length-1)] = 666  # corrupt a couple samples
                        expected[max(24, length-1)] = 666
                    else:
                        expected = expected / 2.0


                    
                start = start + advance  # prepare next frame                
                # Check that frame values match expected frame
                if not np.allclose(f, expected):
                    # Note problem
                    badidx = np.where(np.isclose(f, expected))
                    print('frame {}: '.format(frameidx), end="")
                    if len(badidx) == length:
                        print("Entire frame different")
                        
                        print('READ    {}...{}\nEXPECTED {}...{}\n'.format(
                            f[0:showN], f[-showN:], expected[0:showN], expected[-showN:]))
                    else:
                        print("Differences at: {}".format(", ".join([str(v)for v in badidx])))
                        print("\nREAD    {}\nEXPECTED {}".format(
                            ", ".join([str(f[v]) for v in badidx]),
                            ", ".join([str(expected[v]) for v in badidx])))
                            
                    print('frame {}\nREAD    {}\nEXPECTED {}\n'.format(frameidx, f, expected))
                    try:
                        failed[fileidx].append(frameidx)
                    except KeyError:
                        failed[fileidx] = [frameidx]

                #print("frame {}:  {}\n".format(frame, f))
                frameidx = frameidx + 1
            
            fileidx = fileidx + 1
            
            print ("Frames tested = {}, lengths {} offset_s {}".format(frameidx, framelens, offsets))
            if len(framelens)!= len(framer):
                print("Expected {} frames, iterated over {}".format(len(framer), len(framelens)))
                success = False
            
        if len(failed) > 0:
            print("Frame failures")
            for k, v in failed.items():
                print('File {}: bad frames: '.format(self.testfiles[k]), end="")
                print('{} \n'.format(", ".join([str(block) for block in v])))
            success = False

        print("Test {}:  {}".format(interface, "passed" if success else "failed"))

        return success
    
    def test_AudioFrames_scipy(self):
        return self.checkAudioFrames(interface="wavfile")
    
    def test_AudioFrames_libsnd(self):
        if not soundfile_avail:
            print("soundfile module not available, cannot test")
            return False
        else:
            return self.checkAudioFrames(interface="SoundFile")
        
    def test_Audioframes_libsnd_partial(self):
        if not soundfile_avail:
            print("soundfile module not available, cannot test")
            return False
        else:
            return self.checkAudioFrames(adv_ms=5, len_ms=15, interface="SoundFileStream", incomplete=True)

# class TestStreamer(unittest.TestCase):
#     wavefiles = ["test_wav/sequence{0:0>2}.wav".format(d) for d in range(5)]
#     
#     def test_streamers(self):
#         
#         success = True  # Aren't we the optimist?
#         
#         # Create streamers
#         streams = []
#         for w in self.wavefiles:
#             f = AudioFrames(w, adv_ms=7, len_ms=15, incomplete=True, interface="SoundFileStream")
#             streams.append(f)
# 
#         # Expected length and advance
#         len_N = streams[0].get_framelen_samples()
#         adv_N = streams[0].get_frameadv_samples()
#         
#         streamer = MultiStream(streams)
#         streamit = iter(streamer)
#         
#         start = 1   # expected start value
#         idx = 0
#         for [frame, offset_s, tstamp] in streamit:
#             frame = frame * np.power(2, 15)   # scale up
#             stop = start + len_N - 1
#             if frame[0] != start or frame[-1] != stop:
#                 success = False     # boo who!
#                 print("Frame BAD {} read {}-{}, expected {}-{}".format(
#                     idx, frame[0], frame[-1], start, stop))
#             else:
#                 print("Frame GOOD {} read {}-{}, expected {}-{}".format(
#                     idx, frame[0], frame[-1], start, stop))
#             start = start + adv_N
#             idx = idx + 1
#             
#         
#         return success
                
class TestSampleFile(unittest.TestCase):
    wavefiles = ["test_wav/sequence{0:0>2}.wav".format(d) for d in range(5)]

    def test_SampleFile(self):
        
        s = dsp.SampleStream.SampleFile(('2017-01-01T00:00:00',
            self.wavefiles[0], ['2017-01-01', '2017-01-02'], [440, np.NaN]))
        
        while True:
            try:
                data = s.read(100)
                print(data[1:])
                print(data[0][[0,-1],:]*np.power(2,15))
            except StreamGap as e:
                print("stream gap")
            except EOFError as e:
                print("reached end of file")
                break

    def test_SampleFileOverlapped(self):
        read_new = 110
        read_old = 110
        s = dsp.SampleStream.SampleFile(('2017-01-01T00:00:00',
            self.wavefiles[0], ['2017-01-01', '2017-01-02'], [read_new]))
        
        # priming read
        data = s.read(read_new)
        self.check_samples(data, [0, read_new], scale=np.power(2,15))
        while True:
            try:
                data = s.read(read_new)
                print(data[1:])
                print(data[0][[0,-1],:]*np.power(2,15))
            except StreamGap as e:
                print("stream gap")
            except EOFError as e:
                print("reached end of file")
                break
        
    def check_samples(self, read, expected, scale=None):
        pass
        
        
        
        
class TestSampleStream(unittest.TestCase):
    wavefiles = ["test_wav/sequence{0:0>2}.wav".format(d) for d in range(5)]

    def test_SampleStream(self):
        print()
        
        # Get information about test files
        current = pd.Timestamp('2017-01-01')  # made up date
        stream_elements = dsp.SampleStream.Streams()
        idx = 0
        samples = []
        fs = []
        for w in self.wavefiles:
            # Get information on wavefile
            s = soundfile.SoundFile(w)            
            fs.append(s.samplerate)
            samples.append(len(s))
            del s
            
            if idx == 3:
                # create artificial stream gap
                current = pd.Timestamp('2017-01-02')

            # Add in file, # samples, and start time            
            stream_elements.add_file(w, [samples[-1]], [current], fs[-1])
            
            # Determine next start time
            time_in_file = dsp.timeindex.time_delta_s(samples[-1] / fs[-1]) 
            current = current + time_in_file
            idx = idx + 1
            
        stream = dsp.SampleStream.SampleStream(stream_elements)
        
        advN = 150
        lenN = 300
        overlapN = lenN - advN
        
        idx = 0
        valid = True
        offset = 0  # sample offset
        while True:
            try:
                (data, frame_idx, tstamp, contig_idx, stream_idx) = \
                    stream.read(advN, overlapN)                
                valid = valid and self.check_idx(stream, idx, offset, data, advN, lenN)
                idx = idx + 1
                last_sample = data[-1]
            except dsp.abstractstream.StreamEnd as e:                
                print(e)
                break
            except dsp.abstractstream.StreamGap as e:
                print("Resetting index within stream")
                offset = last_sample
                idx = 0
                print(e)
                
    
    def check_idx(self, stream, idx, offset, data, advN, lenN):
        okay = True
        
        if offset > 0:
            offset = offset * np.power(2, 15)
        
        start = offset + idx * advN
        
        event = stream.get_stream_event()
        if event:
            lenN = data.shape[0]
            truncated = True
        else:
            truncated = False
        
        expected = np.arange(start+1, start+lenN+1).reshape([lenN,1])

        # Convert data to ints
        d2 = data * np.power(2, 15)
        if not np.array_equal(expected, d2):
            print('Error frame {}, received {} expected {}'.format(
                idx, d2[[0,-1],:], [expected[0], expected[-1]]))
            okay = False
        else:
            print("Framed {}: {} - {} good {}".format(
                idx, d2[0,:], d2[-1,0], 
                "truncated read" if truncated else ""))
                    
        return okay
            
        
class TestDSP(unittest.TestCase):
    wavefiles = ["test_wav/ThreeKindsOfLies.wav"]
    
    def test_rms(self):
        
        rms = []
        times = []
        
        # create an RMS stream and iterator
        framer = AudioFrames(self.wavefiles[0], 10, 20)
        rmser = RMSStream(framer)
        rms_it = iter(rmser)

        for r, t, tstamp in rms_it:
            rms.append(r)
            times.append(t)
            
        time = np.asarray(times)
        dB = np.asarray(rms)
            
        plt.figure()
        plt.plot(time, dB)
        plt.xlabel('time (s)')
        plt.ylabel('RMS (dB)')
        #plt.show()

        return True
    
    def test_specgram(self):
        
        specgram = []
        times = []
        
        # create an RMS stream and iterator
        framer = AudioFrames(self.wavefiles[0], 10, 20)
        dftstream = DFTStream(framer)
        dft_it = iter(dftstream)

        for d, t, tstamp in dft_it:
            specgram.append(d)
            times.append(t)
            
        dB = np.column_stack(specgram)
        Hz = dftstream.get_Hz()
        
        #dB = np.asarray(rms)
        pass
            
        plt.figure()
        plt.imshow(dB, extent=(times[0], times[-1], Hz[0], Hz[-1]), origin="lower", aspect='auto')
        plt.xlabel('time (s)')
        plt.ylabel('Hz (dB rel.)')
        plt.colorbar()
        #plt.show()
        
        pass
    
if __name__ == '__main__':
    plt.ion()  # interactive mode on
    
    testall = True
    if testall:
        unittest.main()
    else:
        # specific tests only
        suite = unittest.TestLoader().loadTestsFromTestCase(TestSampleStream)
        unittest.TextTestRunner(verbosity=2).run(suite) 
        