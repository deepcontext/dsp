
data = ([1:5000] / 2^15)';
modN = 1000;
Fs = 8000;

% prime loop
idx = 0;
start = 1;
stop = start+modN - 1;
% write segments of sequence
while stop <= length(data)
    name = sprintf('sequence%02d.wav', idx);
    values = data(start:stop);
    minmax(values')
    audiowrite(name, values, Fs);
    start = start + modN;
    stop = start + modN - 1;
    idx = idx + 1;
end