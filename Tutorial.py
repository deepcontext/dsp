#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 09:51:52 2018

@author: kpalmer
"""

import numpy as np
import pandas as pd
import os, datetime, re
from soundfile import SoundFile
from dsp.SampleStream import SampleStream, Streams



#################################################################
# Define a useful little helper function that returns the start time of
# a given file 
#################################################################
def get_start_timestamp(f, format_str =  "%Y%m%d_%H%M%S"):
    ''' 
    Given a filename (f) returns the datetime of the start of the file
    
    Input:
        f - filename
        format_str = output format desired (default "%Y%m%d_%H%M%S")
    '''
    
    fname= os.path.split(f)[1]
    
    match_date = re.search(r'\d{8}_\d{6}', fname)
    try:
        # Cornell Format
        start_time = datetime.datetime.strptime(match_date.group(), format_str)
    except AttributeError:
        try:# not cornell format try scripps format
         match_date = re.search(r'\d{6}_\d{6}', fname)
         start_time = datetime.datetime.strptime(match_date.group(), format_str)
     
        except AttributeError:
            print('format not recognised- figure out how to implement optional \
                  recursion as an input. Marie...help!')
  
    return(start_time)
    




#################################################################
# Define a function that makes returns a class object containing 
#################################################################
  
''' 
    This section represents is the meat of the problem. We need to load and 
    iterate through all of the sound files in the folder.
    
'''    
    


def MakeSoundStream(DaySoundPath, format_str = "%Y%m%d_%H%M%S"):
    ''' 
    Function for making a soundstream capable of iterating through lots of files.

    Input:
        DaySoundPath - List of folder location(s) containing soundfiles
        format_str - format string of the date default is"%Y%m%d_%H%M%S"
        
    Returns
        Returns a soundstrem of all wav or aif files listed in the folder 
    '''
 
    
    # Declare the stream
    # A Streams object describes the files that will be streamed together
    # It needs information about the size of the files and when they start
    stream_elements = Streams()
    
    
    for ii in range(len(DaySoundPath)):
        # get the file director
        file_dir = DaySoundPath[ii]
        

        # Iterate through the folders and extract associated
        for filename in os.listdir(file_dir):
            
            # if soundfile add it to the stream
            if filename.endswith(".wav") or filename.endswith(".aif"): 
                sound_fullfile = file_dir + '/' + filename
                
                #Get the start time of the sounfile
                start = get_start_timestamp(filename)
                
                # temp file to determine sampele rate and lenght
                
                aa = SoundFile(sound_fullfile)
                # To add a soundfile to the soundfile stream reqires
                # 1) File location
                # 2) number of samples in the file
                # 3) starttime (datetime object) of the file
                # 4) sample rate of the soundfile
                # All of these are stored as meta data and ar not
                
                # Call stream_elements.add() to add each soundfile to the stream 
                
                # Each file in the stream will need to know the name of the file,
		# a list of the lengths of contiguous blocks of samples (in samples)
		# and a list of the start times for each contiguous block.  This permits
		# us to know about discontinuities in the file such as duty cycles.
		# Finally, we specify the sample rate of a file.
                stream_elements.add_file(sound_fullfile,
                                         [len(aa)], [start], aa.samplerate)
                # print(os.path.join(directory, filename))
            else:
                continue

    # Once all stream elements are loaded we define the stream as a 
    # 'SampleStream' class which has it's own methods associated with it            

    stream = SampleStream(stream_elements)
        
    return stream
        




if __name__ == "__main__":    
        

    
    # Directory of sound files- comment and add your own directory
    file_dir = '/cache/kpalmer/quick_ssd/data/Corrnell Data/'+\
        'NARW_analyst_handbrowsed_truth_set/__Sound_BOEM_VA_Historical/'+\
        'BOEM_VA_Historical_20150108'
    
    
    # Declare the stream which will contain a list of all of the soundfiles
    stream_elements = Streams()
    
     # Iterate through the folders and load all soundfiles into the class
    for filename in os.listdir(file_dir):
        
        # if soundfile add it to the stream
        if filename.endswith(".wav") or filename.endswith(".aif"): 
            
            # Define the full soundfiel name and path and declare the soundfile
            sound_fullfile = file_dir + '/' + filename
            aa = SoundFile(sound_fullfile)
            
            # GEt the start time of the sounfile
            start = get_start_timestamp(filename)
            
            # To add a soundfile to the soundfile stream reqires
            # 1) File location
            # 2) number of samples in the file
            # 3) starttime (datetime object) of the file
            # 4) sample rate of the soundfile
            # All of these are stored as meta data and ar not
            
            # Call stream_elements.add() to add each soundfile to the stream 
            stream_elements.add_file(sound_fullfile, # (1)
                                     [len(aa)], # (2)
                                     [start],  # (3)
                                     aa.samplerate) # (4)
            # print(os.path.join(directory, filename))
    
    
    # Declare samplestream
    soundfilestream = SampleStream(stream_elements)
    
    ## Use MakeSounStream to load files from multiple folders ###
    
    # Another way we could do this would be to use the 'MakeSoundStream' 
    # function from above. In doing so we can add all the files in the
    # all the day sound filders
    def listdir_fullpath(d):
        return [os.path.join(d, f) for f in os.listdir(d)]

    DaySoundPath= '/cache/kpalmer/quick_ssd/data/Corrnell Data/'+\
        'NARW_analyst_handbrowsed_truth_set/__Sound_BOEM_VA_Historical/'

    DaySoundPath_list =listdir_fullpath(DaySoundPath)
    soundfilestream_multidir = MakeSoundStream(DaySoundPath_list, 
                                      format_str = "%Y%m%d_%H%M%S")


    # Now that we have a sample stream what can we do with it?
    
    # first, ask how many samples are in the stream in total?
    sum(soundfilestream.get_all_stream_samps())
    
    # Now lets move to the 22 minutes into the file
    soundfilestream.set_sample(22*60*2000)
    
    
    # lets check where the the class thinks it is in the streamer
    soundfilestream.get_current_timesamp()
    
    # Check which file in the the sample stream we are on (should be the
    # second file or index 1) 
    soundfilestream.stream_idx
    
    # Lets move the pointer using date time format, pick a time and add 200 sec
    target_time = soundfilestream.get_current_timesamp() + \
        pd.to_timedelta(200, unit = 'm') 
        
    soundfilestream.set_time(target_time)
    
    # check that it went where we thought in both time and stream index
    soundfilestream.get_current_timesamp()
    soundfilestream.stream_idx
    
    # Lets try moving the pointer in the big stream to a time when there is 
    # a stream gap. Should throw an error
    target_time = soundfilestream_multidir.get_current_timesamp() + \
        pd.to_timedelta(1.1, unit = 'd') 
        
    soundfilestream.set_time(target_time)
    
    

    
    # Read 2 seconds of data from the soundfilestream
    # oh no, I forgot the sample rate of the files so I don't know how many
    # samples to read !
    
    fs_list = soundfilestream.get_all_stream_fs()
    
    # OH good, they are all 2khz so it's just 2000 samples so just read the
    # streamer
    
    samples = soundfilestream.read(2000)
    
    # Which files did I use again to build the streamer?
    filenames= soundfilestream.get_all_stream_filenames()
    
    
    # Iterate through the streamer 10 times advancing 0.1 seconds each time
    # Overlap is 1.9 seconds so we need to tell the streamer to load
    # 2000 samples but of those 1800 should be from the previous read
    
    for ii in range(10):
        
        #soundfile read returns a tuple with 5 elements
        overlap_samples = soundfilestream.read(1800, previousN=200)
        
        #1) samples- numpy array
        #2) starting index (sample) within the current contiguous block
        #3) timestamp of the start of the samples
        #4) index of contiguous block
        #5) something else
        
        # print the current timestamp so we can be sure we are doing what we
        # think we are doing
        print(soundfilestream.get_current_timesamp())
    
    

    # Just the samples from the first channel
    chan1_samps = overlap_samples[0][:,0]
    
    
    
    
    
    
    
    
    