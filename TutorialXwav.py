"""
Example of using extended wav format files
"""
import dsp.AudioStreamDescriptor
import dsp.SampleStream
import dsp.FrameStream
import dsp.DFTStream
from dsp.abstractstream import StreamGap, StreamEnd

import matplotlib.pyplot as plt

# List of filenames to be added, here we just show one.
filenames  = ["test_wav/GofMX_DT08_141007_000000.x.wav"]


stream_descriptors = dsp.SampleStream.Streams()
for f in filenames:
    # Read information about file, use WAVhdr(f) for standard
    # .wav files.  Extended wave files have embedded timing information
    # wave file times are usually derived from the filename, see
    # dsp.AudioStreamDescriptor.WAVhdr for details.
    hdr = dsp.AudioStreamDescriptor.XWAVhdr(f)
    # Add information to the stream descriptor
    # We unpack the tuple returned as these are the arguments
    # expected by add_file
    stream_descriptors.add_file(*hdr.get_stream_descriptor())

# We now have a sample stream that we can iterate over, seek etc.
# We can seamlessly iterate over multiple files, treating them as if they were
# one.
#
# When iterating, if we get to a gap in the stream due to duty cycled
# data, dropout, etc. we generate a StreamGap exception.
#
sample_stream = dsp.SampleStream.SampleStream(stream_descriptors)

# We can build on this.  For example, if we wanted streams of spectra,
# we can add a framer stream and DFT stream on top of it.

# Iterating over framer produces one data frame at a time with the specified
# characteristics
framer = dsp.FrameStream.AudioFrames(sample_stream, adv_ms=10, len_ms=20)

# The DFTStream expects a frame stream and will compute a DFT on each of these
# By default, it returns dB up to Nyquist, but initialization parameters can
# specify
spectra = dsp.DFTStream.DFTStream(framer)
bins_Hz = spectra.get_Hz()  # frequency bin labels

plt.figure()

# Do something interesting,
# here we will simply compute the mean spectra (not so interesting)
cum = None
count = 0
# If we know that there are no gaps in the file, we can just use a for loop
# for dft in spectra ...
# but we will compute mean spectra for each contiguous segment
frame_iter = iter(spectra)
more = True
legends = [frame_iter.get_current_timestamp()]
while more:
    try:
        dft, t, tstamp = next(frame_iter)
        count = count + 1
        if cum is None:
            cum = dft  # first time
        else:
            cum = cum + dft  # accumulate
    except StreamEnd:
        more = False  # All done
        plot(bins_Hz, cum / count) # Plot average spectra
    except StreamGap as gap:
        plot(bins_Hz, cum / count) # Plot average spectra
        # gap contains start time of next segment
        legends.append(gap.timestamp)
        # Rest for next block
        count = 0
        cum = None

print('all done')

