
Additional modules installed:

conda install pandas
pip install orderedcontainers
pysoundfile - needs to be installed from source pip version missing things

This dsp library is intended for audio data.

Most modules are inside the dsp package.

Samples of how the classes are used can be seen in the unit test file dsptests.py

General idea:

AudioFrames class provides a stream of framed audio samples that can be iterated over.
These can be used directly or the object can be passed to another class to derive something
from it.  AudioFrames is designed to work with a single file.

Example:
from dsp.AudioFrames import AudioFrames

framer = AudioFrames("file.wav", adv_ms = 5, len_ms = 15)
for f in framer:
	do_something(f)  # process samples in frame f


RMSStream and DFTStream take an AudioFrames object as as input and produce 
root mean square energy or discrete Fourier transform outputs as they are
iterated over.


Example:
from dsp.AudioFrames import AudioFrames
from dsp.DFTStream import DFTStream

framer = AudioFrames("file.wav", adv_ms = 5, len_ms = 15)
dfts = DFTStream(framer)
for dft in dfts:
	do_something(dft)  # process spectrum in current frame (RMSStream is similar)

To handle multiple files, use dsp.SampleStream.  This allows us to build a group
of files to be processed as a stream.  It requires the user to know (or write a function 
to obtain) information about the start time of a file.  All files in a stream are expected
to have the same sample rate, bit width, and number of channels, but they need not be
contiguous.  Duty cycled data is not yet supported, but can be added within the current 
framework.

For example, suppose we had a set of files in variable files.

from dsp.SampleStream import SampleStream, Streams
from soundfile import SoundFile

# Build the list of files to be streamed
stream_elements = Streams()
for f in files:
	start = get_start_timestamp(f)  # you need to write this
	# Get sample rate & number of samples in file
	# for the example, we'll assume that the data are contiguous
	s = Soundfile(f)
	stream_elements.add_file(f, [s.frames], [s.samplerate])

# Construct a multiple file stream
stream = SampleStream(stream_elements)

This can now be iterated over, passed to other streams (e.g. DFTStream) etc.
This class throws two types of iteration exceptions defined in dsp/abstractstream.py:

StreamGap - This is thrown whenever one tries to read data that would result in a
noncontiguous data read.  Reading again will produce the new data.  The field
timestamp contains the timestamp associated with the next read.

StreamEnd - This is a subclass of EOFError and is called when we have read the last
data from the last file in a set of files.

